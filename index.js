/** **/
const linq = require("@altronix/linq"),
  nodemailer = require("nodemailer"),
  util = require("util"),
  fs = require("fs");

let config;
let mailer;

// Read user mailer deamon configuration
try {
  let p = process.argv.length == 3 ? process.argv[2] : "./sample-config.json";
  config = JSON.parse(fs.readFileSync(p));
} catch (e) {
  console.log("\n\n");
  console.log("CONFIG FILE FORMAT ERROR!!!");
  console.log("SEE sample-config.json FOR FORMAT DETAILS");
  console.log("PROGRAM TERMINATING!!!");
  console.log("\n\n");
  process.exit(-1);
}

mailer = nodemailer.createTransport(config.mailer);

function sendMail(device, message, mail) {
  let m = {
    from: config.mailer.auth.user,
    to: [],
    subject: "",
    html: ""
  };
  mail = message.alert || mail;
  m.to = Object.keys(mail)
    .filter(key => key.substr(0, 2) == "to" && mail[key].length)
    .map(key => mail[key]);
  m.html = util.format(
    "" +
      "<ul>" +
      "<li>Serial: [%s]</li>" +
      "<li>Product: [%s]</li>" +
      "<li>Who: [%s]</li>" +
      "<li>What: [%s]</li>" +
      "<li>Site ID: [%s]</li>" +
      "<li>When: [%d]</li>" +
      "<li>Message: [%s]</li>" +
      "</ul>",
    device.serial(),
    device.product(),
    message.who,
    message.what,
    message.siteId,
    message.when,
    message.mesg
  );
  mailer.sendMail(m, err => console.log(err ? err : "Mail Sent!"));
}

// Start LinQ - Listen for Altronix Device
linq
  .on("error", e => console.log(e))
  .on("new", d => console.log("[%s] new device", d.serial()))
  .on("alert", (device, message, mail) => sendMail(device, message, mail))
  .listen(config.port)
  .then(() => console.log("LinQ Ready!"))
  .catch(e => console.log(e));
